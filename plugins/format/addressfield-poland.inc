<?php

/**
 * @file
 * A specific handler for Poland.
 */

$plugin = array(
  'title' => t('Polish address form'),
  'format callback' => 'address_field_format_address_pl_generate',
  'type' => 'address',
  'weight' => 10,
);

function address_field_format_address_pl_generate(&$format, $address, $context = array()) {
  if($address['country'] == 'PL') {
    // A list of polish administrative divisions.
    $format['locality_block']['administrative_area'] = array(
      '#title' => t('State'),
      '#size' => 10,
      '#required' => TRUE,
      '#prefix' => ' ',
      '#attributes' => array('class' => array('state')),
      '#options' => array(
        ''   => t('--'),
        'dolnośląskie' => t('dolnośląskie'),
        'kujawsko-pomorskie' => t('kujawsko-pomorskie'),
        'lubelskie' => t('lubelskie'),
        'lubuskie' => t('lubuskie'),
        'łódzkie' => t('łódzkie'),
        'małopolskie' => t('małopolskie'),
        'mazowieckie' => t('mazowieckie'),
        'opolskie' => t('opolskie'),
        'podkarpackie' => t('podkarpackie'),
        'podlaskie' => t('podlaskie'),
        'pomorskie' => t('pomorskie'),
        'śląskie' => t('śląskie'),
        'świętokrzyskie' => t('świętokrzyskie'),
        'warmińsko-mazurskie' => t('warmińsko-mazurskie'),
        'wielkopolskie' => t('wielkopolskie'),
        'zachodniopomorskie' => t('zachodniopomorskie'),
      ),
    );

    $format['street_block']['thoroughfare']['#title'] = t('Street');
    $format['street_block']['thoroughfare']['#tag'] = 'span';

    $format['street_block']['premise']['#title'] = t('Building number');
    $format['street_block']['premise']['#size'] = 8;
    $format['street_block']['premise']['#tag'] = 'span';

    $format['street_block']['sub_premise'] = array(
      '#title' => t('Flat number'),
      '#attributes' => array('class' => array('sub-premise')),
      '#size' => 8,
      '#tag' => 'span',
    );

    $format['street_block']['#attributes']['class'][] = 'addressfield-container-inline';

    if($context['mode'] == "render") {
      if($address['sub_premise']) {
        $format['street_block']['premise']['#suffix'] = "/";
      }
      $format['street_block']['thoroughfare']['#suffix'] = ' ';
      if($format['#handlers']['address-hide-country']) {
        unset($format['country']);
      }
      if(empty($address['postal_code'])) {
        $format['locality_block']['locality']['#prefix'] = '';
      }
    }
  }
}
